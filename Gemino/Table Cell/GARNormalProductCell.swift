//
//  GARNormalProductCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 11/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import Firebase
import InteractiveSideMenu
import Vision
import MobileBuySDK
import SDWebImage
import SceneKit

class GARNormalProductCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    @IBOutlet weak var colCell: UICollectionView!
    var banners = [Storefront.CollectionEdge]()
    //var arrHome = ["Small Banner", "Medium Banner", "Big Banner", "Animated Product" ,"Simple Product", "Circle"]
    
  //  var arrHome = ["Circle", "Small Banner","Animated Product" , "Medium Banner",  "Simple Product","Big Banner"]

  //  var arrProduct = ["Small Banner" : "Ad", "Medium Banner" : "Coupon", "Big Banner" : "Offers", "Animated Product" : "New Arrival" ,"Simple Product" : "Discount", "Circle" : "Collection"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        colCell.dataSource = self
        colCell.delegate = self
     //   colCell.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
   //     colCell.register(UINib(nibName: "GARBannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARBannerCollectionCell")
        colCell.register(UINib(nibName: "GARProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARProductCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARProductDetailsPageViewController") as? GARProductDetailsPageViewController {
            viewController.product = banners[0].node.products.edges[indexPath.row]
            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            navigationController.pushViewController(viewController, animated: true)
        }
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            break
//        case "Medium Banner":
//            break
//        case "Big Banner":
//            break
//        case "Animated Product":
//            break
//        case "Simple Product":
//            break
//        case "Circle":
//            break
//        default:
//            break
//        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        switch arrHome[collectionView.tag] {
//
//        case "Small Banner":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//        case "Medium Banner":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//        case "Big Banner":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//
//
//        case "Animated Product":
//         //   lblTitle.text = "New Arrival"
//            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
//            collectionView.decelerationRate = UIScrollViewDecelerationRateFast
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
//            return cell
//
//        case "Simple Product":
//         //   lblTitle.text = "Discount"
            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
            collectionView.decelerationRate = UIScrollViewDecelerationRateFast
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
        cell.imgProduct.sd_setImage(with: banners[0].node.products.edges[indexPath.row].node.images.edges.last!.node.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
        cell.lblProductName.text = banners[0].node.products.edges[indexPath.row].node.title
        cell.lblProductPrice.text = "\(banners[0].node.products.edges[indexPath.row].node.priceRange.maxVariantPrice.amount) \(banners[0].node.products.edges[indexPath.row].node.priceRange.maxVariantPrice.currencyCode)"
            return cell
            
//        case "Circle":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
//
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARCircleCollectionCell", for: indexPath) as! GARCircleCollectionCell
//            return cell
//        default:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
//            return cell
//        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // switch arrHome[collectionView.tag] {
            
//        case "Small Banner":
//            print("Small Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Medium Banner":
//            print("Medium Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Big Banner":
//            print("Big Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Animated Product":
//            print("Animated Product \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Simple Product":
          //  print("Simple Product \(arrHome[collectionView.tag])")
        if(self.banners.count > 0)
        {
            return self.banners[0].node.products.edges.count
        }
        return 0
            
//        case "Circle":
//            print("Circle \(arrHome[collectionView.tag])")
//            return 10
//
//        default:
//            return 0
//            //        case "Collection":
//            //
//            //        case "Big Banner":
//            //            return 10
//            //        case "New Arrival":
//            //            return 10
//            //        case "Discount":
//            //            return 10
//            //        default:
//            //            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return 0
//
//        case "Medium Banner":
//            return 0
//
//        case "Big Banner":
//            return 0
//
//        case "Animated Product":
//            return 0
        
     //   case "Simple Product":
            return 20
            
//        case "Circle":
//            return 50
//
//        default:
//            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
     //   switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return 0
//
//        case "Medium Banner":
//            return 0
//
//        case "Big Banner":
//            return 0
//
//        case "Animated Product":
//            return 0
//
//        case "Simple Product":
            return 20
            
//        case "Circle":
//            return 50
//
//        default:
//            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Medium Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Big Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Animated Product":
//            return CGSize(width: collectionView.frame.width/2, height: 300)
//
//        case "Simple Product":
            return CGSize(width: collectionView.frame.width/3, height: 300)
            
//        case "Circle":
//            return CGSize(width: 80, height: 80)
//
//        default:
//            return CGSize(width: 0, height: 0)
//        }
    }
    func reloadData(banner : [Storefront.CollectionEdge])
    {
        self.banners = banner
        colCell.reloadData()
    }
}
