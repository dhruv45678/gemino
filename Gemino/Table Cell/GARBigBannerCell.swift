//
//  GARBigBannerCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 11/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import Firebase
import InteractiveSideMenu
import Vision
import MobileBuySDK
import SDWebImage
import SceneKit

class GARBigBannerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    @IBOutlet weak var colCell: UICollectionView!
    var banners = [Storefront.CollectionEdge]()
    //var arrHome = ["Small Banner", "Medium Banner", "Big Banner", "Animated Product" ,"Simple Product", "Circle"]
    var arrHome = ["Circle", "SmallBanner" ,"AnimatedProduct" ,"MediumBanner" ,"SimpleProduct" ,"BigBanner" ,"Grid", "SmallBanner", "SmallBanner"]

    var arrProduct = ["Small Banner" : "Ad", "Medium Banner" : "Coupon", "Big Banner" : "Offers", "Animated Product" : "New Arrival" ,"Simple Product" : "Discount", "Circle" : "Collection"]
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        colCell.dataSource = self
        colCell.delegate = self
   //     colCell.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
        colCell.register(UINib(nibName: "GARBannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARBannerCollectionCell")
      //  colCell.register(UINib(nibName: "GARProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARProductCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARProductGridViewController") as? GARProductGridViewController {
            viewController.isBackButton = true
            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            navigationController.pushViewController(viewController, animated: true)
            }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
        cell.imgBanner.sd_setImage(with: banners[0].node.products.edges[indexPath.row].node.images.edges.last!.node.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.banners.count > 0)
        {
            return self.banners[0].node.products.edges.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 300)
    }
    func reloadData(banner : [Storefront.CollectionEdge])
    {
        self.banners = banner
        colCell.reloadData()
    }
}
