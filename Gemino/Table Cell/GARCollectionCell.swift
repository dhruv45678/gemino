//
//  GARCollectionCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 07/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import Firebase
import InteractiveSideMenu
import Vision
import MobileBuySDK
import SDWebImage
import SceneKit

class GARCollectionCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    @IBOutlet weak var colCell: UICollectionView!
    var banners = [Storefront.CollectionEdge]()
    
    //var arrHome = ["Collection" , "Big Banner", "New Arrival", "Discount"]
  //  var arrHome = ["Small Banner", "Medium Banner", "Big Banner", "Animated Product" ,"Simple Product", "Circle"]
    var arrHome = ["Circle", "Small Banner","Animated Product" , "Medium Banner",  "Simple Product","Big Banner"]

    var arrProduct = ["Small Banner" : "Ad", "Medium Banner" : "Coupon", "Big Banner" : "Offers", "Animated Product" : "New Arrival" ,"Simple Product" : "Discount", "Circle" : "Collection"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        colCell.dataSource = self
        colCell.delegate = self
        colCell.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
       // colCell.register(UINib(nibName: "GARBannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARBannerCollectionCell")
       // colCell.register(UINib(nibName: "GARProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARProductCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        // Safe Push VC
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARProductGridViewController") as? GARProductGridViewController {
            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            navigationController.pushViewController(viewController, animated: true)
            
        //    self.window?.rootViewController?.navigationController?.pushViewController(viewController, animated: true)
        }
        
        //let controller = mainStoryboard.instantiateViewController(withIdentifier: "GARProductDetailsPageViewController") as! GARProductDetailsPageViewController
        //self.navigationController?.pushViewController(controller, animated: true)
        
//        switch arrHome[collectionView.tag] {
//
//        case "Small Banner":
//            break
//        case "Medium Banner":
//            break
//        case "Big Banner":
//            break
//        case "Animated Product":
//            break
//        case "Simple Product":
//            break
//        case "Circle":
//            break
//        default:
//            break
//        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        switch arrHome[collectionView.tag] {
//
//        case "Small Banner":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//        case "Medium Bannerr":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//
//        case "Big Banner":
//            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//        case "Animated Product":
//            updateCellsLayout()
//            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
//            collectionView.decelerationRate = UIScrollViewDecelerationRateFast
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
//            return cell
//
//        case "Simple Product":
//            //updateCellsLayout()
//
//            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
//            //colCell.decelerationRate = UIScrollViewDecelerationRateFast
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
//            return cell
//
//        case "Circle":
            collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARCircleCollectionCell", for: indexPath) as! GARCircleCollectionCell
        
        cell.imgCollection.sd_setImage(with: banners[indexPath.row].node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
        
        cell.lblCollection.text = banners[indexPath.row].node.title
        
            GARHelper().roundCorner(view: cell.imgCollection, corner: cell.imgCollection.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
          //  cell.lblCollection.text =
          //  cell.imgCollection.image = banners[0].node.products.edges[indexPath.row].node.title
      //  cell.lblProductPrice.text = "\(banners[0].node.products.edges[indexPath.row].node.priceRange.maxVariantPrice)"
            return cell
            
//        default:
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
//            return cell
//        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            print("Small Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Medium Banner":
//            print("Medium Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Big Banner":
//            print("Big Banner \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Animated Product":
//            print("Animated Product \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Simple Product":
//            print("Simple Product \(arrHome[collectionView.tag])")
//            return 10
//
//        case "Circle":
          //  print("Circle \(arrHome[collectionView.tag])")
            return self.banners.count
//        default:
//            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Medium Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Big Banner":
//            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
//
//        case "Animated Product":
//            return CGSize(width: collectionView.frame.width/2, height: 300)
//
//        case "Simple Product":
//            return CGSize(width: collectionView.frame.width/2, height: 300)
        
     //   case "Circle":
            return CGSize(width: 80, height: 80)
            
//        default:
//            return CGSize(width: 0, height: 0)
//
//        }
    }
    
    func updateCellsLayout()  {
        if(arrHome[colCell.tag] == "Animated Product")
        {
            let centerX = colCell.contentOffset.x + (colCell.frame.size.width)/2
        
            for cell in colCell.visibleCells {
                var offsetX = centerX - cell.center.x
                if offsetX < 0 {
                    offsetX *= -1
                }
                cell.transform = CGAffineTransform.identity
                let offsetPercentage = offsetX / (colCell.bounds.width * 2.7)
                let scaleX = 1-offsetPercentage
                cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return 0
//
//        case "Medium Banner":
//            return 0
//
//        case "Big Banner":
//            return 0
//
//        case "Animated Product":
//            return 0
//
//        case "Simple Product":
//            return 20
//
//        case "Circle":
            return 20
            
//        default:
//            return 0
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        switch arrHome[collectionView.tag] {
//        case "Small Banner":
//            return 0
//
//        case "Medium Banner":
//            return 0
//
//        case "Big Banner":
//            return 0
//
//        case "Animated Product":
//            return 0
//
//        case "Simple Product":
//            return 20
//
//        case "Circle":
            return 20
//
//        default:
//            return 0
//        }
    }
    
    func reloadData(banner : [Storefront.CollectionEdge])
    {
        self.banners = banner
        colCell.reloadData()
    }
    
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        colCell.reloadData()
//    }
//    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
////        switch arrHome[colCell.tag] {
////
////        case "Small Banner":
////            colCell.reloadData()
////            break
////
////        case "Medium Banner":
////            colCell.reloadData()
////            break
////
////        case "Big Banner":
////            colCell.reloadData()
////            break
////
////        case "Animated Product":
////            updateCellsLayout()
////            colCell.reloadData()
////            break
////
////        case "Simple Product":
////            colCell.reloadData()
////            break
////
////        case "Circle":
//            colCell.reloadData()
////            break
////            
////        default:
////            colCell.reloadData()
////            break
////        }
//    }
}
