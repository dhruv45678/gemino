//
//  GARARCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 19/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import GoogleMobileVision



class GARARCell: UITableViewCell {

    let faceDetector = GARFaceLandmarksDetector()
    let captureSession = AVCaptureSession()
    @IBOutlet weak var image_View: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureDevice()
    }
    private func getDevice() -> AVCaptureDevice? {
        let discoverSession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInTelephotoCamera, .builtInWideAngleCamera], mediaType: .video, position: .front)
        return discoverSession.devices.first
    }
    
    private func configureDevice() {
        if let device = getDevice() {
            do {
                try device.lockForConfiguration()
                if device.isFocusModeSupported(.continuousAutoFocus) {
                    device.focusMode = .continuousAutoFocus
                }
                device.unlockForConfiguration()
            } catch { print("failed to lock config") }
            
            do {
                let input = try AVCaptureDeviceInput(device: device)
                captureSession.addInput(input)
            } catch { print("failed to create AVCaptureDeviceInput") }
            
            captureSession.startRunning()
            
            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.videoSettings = [String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_32BGRA)]
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .utility))
            
            if captureSession.canAddOutput(videoOutput) {
                captureSession.addOutput(videoOutput)
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
}
extension GARARCell: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        
        
        
        
        
        // Scale image to process it faster
        let maxSize = CGSize(width: 1024, height: 1024)
        //.flipped()?
//        if let image = UIImage(sampleBuffer: sampleBuffer)?.flipped()?.imageWithAspectFit(size: maxSize) {
//            faceDetector.highlightFaces(for: image, glassImage: #imageLiteral(resourceName: "glass_Transperant"), scene: ) { (resultImage) in
//                DispatchQueue.main.async {
//                    self.image_View?.image = resultImage
//                }
//            }
//        }
    }
}
