//
//  GARMenuCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARMenuCell: UITableViewCell {

    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
