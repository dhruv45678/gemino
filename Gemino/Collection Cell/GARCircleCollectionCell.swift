//
//  GARCircleCollectionCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 07/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARCircleCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgCollection: UIImageView!
    @IBOutlet weak var lblCollection: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

}
