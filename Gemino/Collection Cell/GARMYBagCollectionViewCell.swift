//
//  GARMYBagCollectionViewCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARMYBagCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSize: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewSaperator: UIView!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        GARHelper().roundCorner(view: imgProduct, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        // Initialization code
    }

}
