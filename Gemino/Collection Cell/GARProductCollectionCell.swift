//
//  GARProductCollectionCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 07/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARProductCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        GARHelper().roundCorner(view: imgProduct, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        // Initialization code
    }

}
