//
//  GARListCollectionViewCell.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgCollection: UIImageView!
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        GARHelper().roundCorner(view: viewContainer, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        // Initialization code
    }

}
