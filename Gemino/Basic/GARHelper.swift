//
//  GARHelper.swift
//  Gemino
//
//  Created by dhruv Khatri on 08/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import Foundation
import UIKit

class GARHelper {
    
    
    func removeBackground(image : UIImage) -> UIImage
    {
        let rawImageRef: CGImage = image.cgImage!
        let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
        UIGraphicsBeginImageContext((image.size));
        let maskedImageRef=rawImageRef.copy(maskingColorComponents: colorMasking);
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap?.translateBy(x: 0.0, y: (image.size.height));
        bitmap?.scaleBy(x: 1.0, y: -1.0);
        bitmap?.draw(maskedImageRef!, in: CGRect(x: 0 , y: 0, width: (image.size.width), height: (image.size.height)))
        let result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return result!
    }
    
    func roundCorner(view : Any, corner : CGFloat, borderColor : CGColor, borderWidth : CGFloat)
    {
        (view as! UIView).layer.cornerRadius = corner
        (view as! UIView).clipsToBounds = true
        (view as! UIView).layer.borderColor = borderColor
        (view as! UIView).layer.borderWidth = borderWidth
    }
    
    
    func addTItle(view : UIView, value : String)
    {
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: 200, height: 50))
        label.text = value
        view.addSubview(label)
    }
    
    func createAlert(message : String, VC : UIViewController)
    {
        var height = 50
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
           height = 100
        }
        let view = UIView(frame: CGRect(x: 0, y: -height, width: Int(VC.view.frame.width), height: height))
        view.backgroundColor = UIColor.black
        let text = UILabel(frame: CGRect(x: 0, y: height - 50, width: Int(VC.view.frame.width), height: 50))
        text.text = "\(message)"
        text.textAlignment = .center
        text.font = UIFont.systemFont(ofSize: 14.0)
        text.textColor = UIColor.white
        view.addSubview(text)
        VC.view.addSubview(view)
        
        UIView.animate(withDuration: 0.2, animations: {
            view.transform = CGAffineTransform(translationX: 0, y: CGFloat(height))
        }) { (true) in
            Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (timer) in
                UIView.animate(withDuration: 0.2, animations: {
                    view.transform = CGAffineTransform(translationX: 0, y: 0)
                }, completion: { (true) in
                    view.removeFromSuperview()
                })
            })
        }
    }
    
}
