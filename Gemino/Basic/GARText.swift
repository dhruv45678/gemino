//
//  OTPText.swift
//  OTP.AR
//
//  Created by dhruv Khatri on 30/04/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import Foundation
struct Key {
    static let AcceptedPrivacy  = "acceptedPrivacy"
    static let ID               = "id"
    static let Email            = "email"
    static let Password         = "password"
    static let UUID             = "uuid"
    static let UID              = "uid"
    static let Username         = "username"
    static let Location         = "location"
    static let Occupation       = "occupation"
    static let LookingFor       = "lookingFor"
    static let Gender           = "gender"
    static let User             = "user"
    static let AllOccupiedUsers = "allOccupiedUsers"
    static let Image            = "Image"
    static let Bio              = "bio"
    static let Headline         = "headline"
    static let Status           = "status"
    static let Gallery          = "gallery"
    static let ImageURL         = "imageLink"
    static let Color            = "color"
    static let RedColor         = "R"
    static let GreenColor       = "G"
    static let BlueColor        = "B"
    static let ARType           = "arType"
    
    //Tables
    static let APPID            = "APP_ID"
    static let Profile          = "profileValues"
    
    
}
struct OTPErrorMessage {
        let noInternet   = "Internet connection is not available. Please check your internet connection and try again."
        let authorizationFailed = "Handler Failed"
        let unknownError = "Server is not responding due to some error. Please try later."
        
        let cameraError  = "Camera is not available for this device."
        
        let cameraPermissionMsg = "Application doesn't have permission to access camera. To enable access, Tap setting and enable Camera."
        let photoPermissionMsg      = "Application doesn't have permission to access Photos. To enable access, Tap setting and enable Photos."
        let failCurrentLocation  = "We are not able to identify your location. Please go to Settings and turn on Location Services."
        let askLogout            = "Are you sure you want to logout?"
        let locationBlank        = "Please enter your Location."
        let occupationBlank        = "Please enter your Occupation."
        let lookingBlank         = "Please select looking For."
        let emailBlank           = "Please enter your email address."
        let emailInvalid         = "Please enter a valid email address."
        let emailNeeded          = "Please allow email address from Social account to log in."
        let passwordBlank        = "Please enter your password."
        let passwordSpace        = "Please remove spaces from your password."
        let passwordInvalid      = "Please enter a password that has a minimum of six characters."
        let passwordConfirmBlank = "Please enter a confirm your password."
        let passwordConfirmMatch = "Passwords do not match."
        let passwordConfirmSpace = "Please remove spaces from the password you're confirming."
        let passwordConfirmInvalid = "Please enter a confim  password that has a minimum of six characters."
        let contactNoInvalid     = "Please enter a valid Phone Number."
        let contactNoBlank       = "Please enter a Phone Number."
        let nameBlank            = "Please enter your Full name."
        let chooseImageOption    = "How do you want to add an image?"
        let infoTermsOfService   = "by signing up I agree to the Terms of Service for this application."
        let reportSuccess           = "You have reported successfully."
        let fillData             = "Please Fill All Data."
        let userNameNotAvailable          = "Username not available."
        let userNameBlank          = "Please enter your UserName."
        let userNameSpace        = "Please remove spaces from your UserName."
        
        let feedBackToolTip      = "Get a FeedBack email notice every time the user sends an email to the forwarding address."
        let attachmentToolTip   = "Do not use the ‘Include Attachment’ option for the sensitive data of HIPAA compliance."
        let emailAlreadyThere   = "Email is already added in Allowed Senders' List."
        let confirmCondition    = "Please Confirm that you are 18+."
        let selectLookingFor    = "Please select Looking For."
    
}
