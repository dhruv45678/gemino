//
//  OTPExtension.swift
//  OTP.AR
//
//  Created by dhruv Khatri on 27/04/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import Foundation
import UIKit

typealias voidFunc  = ()  -> Void


extension UserDefaults {
    public subscript(key: String) -> Any? {
        get {
            return object(forKey: key) as Any?
        }
        set {
            set(newValue, forKey: key)
            synchronize()
        }
    }
    
    public static func contains(key: String) -> Bool {
        return self.standard.contains(key: key)
    }
    
    public func contains(key: String) -> Bool {
        return self.dictionaryRepresentation().keys.contains(key)
    }
    
    public func reset() {
        for key in Array(UserDefaults.standard.dictionaryRepresentation().keys) {
            UserDefaults.standard.removeObject(forKey: key)
        }
        synchronize()
    }
}

extension UITextField
{
    func setPadding(isLeft : Bool)
    {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: frame.height))
        view.backgroundColor = UIColor.clear
        if(isLeft)
        {
            leftViewMode = .always
            leftView = view
        }
        else
        {
            rightViewMode = .always
            rightView = view
        }
    }
    func setPaddingWith(image : UIImage, isLeft : Bool)
    {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.height, height: frame.height))
        view.backgroundColor = UIColor.clear
        let imageView = UIImageView(frame: CGRect(x: frame.height/2 - 15, y: frame.height/2 - 15, width: 30, height: 30))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        if(isLeft)
        {
            leftViewMode = .always
            leftView = view
        }
        else
        {
            rightViewMode = .always
            rightView = view
        }
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIViewController
{
    //MARK: - ALERT CODE
    func createAlertWithFunction(title : String, message : String, complete : @escaping voidFunc)
    {
        let alert = UIAlertController(title: "\(title)", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in
            complete()
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    func createAlert(title : String, message : String)
    {
        
        let alert = UIAlertController(title: "\(title)", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}
extension UIImage{
    
    func alpha(_ value:CGFloat)->UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }
    
    func removeBackground() -> UIImage
    {
        let rawImageRef: CGImage = self.cgImage!
        let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
        UIGraphicsBeginImageContext((self.size));
        let maskedImageRef=rawImageRef.copy(maskingColorComponents: colorMasking);
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap?.translateBy(x: 0.0, y: (self.size.height));
        bitmap?.scaleBy(x: 1.0, y: -1.0);
        bitmap?.draw(maskedImageRef!, in: CGRect(x: 0 , y: 0, width: (self.size.width), height: (self.size.height)))
        let result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return result ?? self
    }
    
}
