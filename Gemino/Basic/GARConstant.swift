//
//  GARConstant.swift
//  Gemino
//
//  Created by dhruv Khatri on 12/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import UIKit
import MobileBuySDK

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}




struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

enum ViewControllerType {
    case welcome
    case conversations
}

enum PhotoSource {
    case library
    case camera
}

enum ShowExtraView {
    case contacts
    case profile
    case preview
    case map
}

enum MessageType {
    case photo
    case text
    case location
}

enum MessageOwner {
    case sender
    case receiver
}

let Firebase = Database.database().reference()
var AppColor = UIColor.darkGray
var arType   = "2D"
var arrHome  = [String]()
//var shopingDomain = "prettybigcanvas.myshopify.com/"
//https://4515f2ae2f50cf50187378e331fef092:481db0b8a6e40b49237c23a440d6c2b3@prettybigcanvas.myshopify.com/admin/orders.json

var shopingDomain = "prettybigcanvas.myshopify.com"

// "prettybigcanvas.myshopify.com"  //"prettybigcanvas.myshopify.com" //"prettybigcanvas.com" //"smplmrkt.myshopify.com"
var API = "eb3c525debac1bdd4d87afbe0bf17b4c"

//"4515f2ae2f50cf50187378e331fef092"  //"263afee6c3bbd5c74cba4a88b63c0bf3" //"5db7999f831d6263c989e6c4acae543d"
var API_ID = "1" //"8"




let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
var cartProducts = [Storefront.ProductVariantEdge]()

func remove(image : UIImage) -> UIImage
{
    let rawImageRef: CGImage = image.cgImage!
    let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
    UIGraphicsBeginImageContext(image.size);
    let maskedImageRef=rawImageRef.copy(maskingColorComponents: colorMasking);
    let bitmap = UIGraphicsGetCurrentContext()
    bitmap?.translateBy(x: 0.0, y: image.size.height);
    bitmap?.scaleBy(x: 1.0, y: -1.0);
    bitmap?.draw(maskedImageRef!, in: CGRect(x: 0 , y: 0, width: image.size.width, height: image.size.height))
    var result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result!
}
