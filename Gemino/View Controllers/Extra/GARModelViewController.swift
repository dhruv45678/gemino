//
//  GARModelViewController.swift
//  Gemino
//
//  Created by Khatri Dhruv on 16/09/19.
//  Copyright © 2019 dhruv Khatri. All rights reserved.
//

import UIKit

class GARModelViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var colListOfModels: UICollectionView!
    var myList = ["AR Aviator Sunglasses",
                  "AR Bicycle",
                  "AR Black Leather Couch",
                  "AR Black Leather Solo Couch",
                  "AR Brown Leather Couch",
                  "AR Brown Suede L Couch",
                  "AR Cowboy Hat",
                  "AR Cozy Brown Chair",
                  "AR Guitar",
                  "AR Lamp Black",
                  "AR Lamp Blue",
                  "AR Lamp Orange",
                  "AR Lamp Red",
                  "AR Long Marble Table",
                  "AR Pouf Purple",
                  "AR Retro TV",
                  "AR Round Marble Table",
                  "AR Suede Solo Couch",
                  "AR Toy Car",
                  "AR White Triangle Chair",
                  "AR Wood Table Long"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colListOfModels.delegate = self
        colListOfModels.dataSource = self
        colListOfModels.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
        // Do any additional setup after loading the view.
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "GARSurfaceDetectViewController") as! GARSurfaceDetectViewController
        controller.selectedSceneName = myList[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARCircleCollectionCell", for: indexPath) as! GARCircleCollectionCell
        
        cell.imgCollection.image = UIImage(named: "store")
        
        cell.lblCollection.text = myList[indexPath.row]
        
       // GARHelper().roundCorner(view: cell.imgCollection, corner: cell.imgCollection.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        return cell
       
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.myList.count
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

