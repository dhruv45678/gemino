//
//  GARVerticalARSurfaceViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 26/09/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//


import UIKit
import ARKit
import InteractiveSideMenu
class GARVerticalARSurfaceViewController: UIViewController, SideMenuItemContent {
    
    @IBOutlet weak var sceneView: ARSCNView!
    var plane : SCNPlane!
    var isModelAdded : Bool!
    let wrapperNode = SCNNode()
    var selectedImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        isModelAdded = false
        // addTapGestureToSceneView() //UNComment For Chair
        configureLighting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        setUpSceneView()
        //setUpSceneViewForFaceDetection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    func setUpSceneView() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .vertical
        
        sceneView.session.run(configuration)
        
        sceneView.delegate = self
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    func setUpSceneViewForFaceDetection()
    {
        guard ARFaceTrackingConfiguration.isSupported else {
            return
        }
        let configuration = ARFaceTrackingConfiguration()
        configuration.isLightEstimationEnabled = true
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        
    }
    
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    @objc func addShipToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        
        guard let hitTestResult = hitTestResults.first else { return }
        let translation = hitTestResult.worldTransform.translation
        let x = translation.x
        let y = translation.y
        let z = translation.z
        
        //        guard let shipScene = SCNScene(named: "chair.scn"),
        //            let shipNode = shipScene.rootNode.childNode(withName: "chair", recursively: false)
        //            else { return }
        
        //        guard let shipScene = SCNScene(named: "chair.scn", inDirectory: "Models.scnassets/chair") else {
        //           print("Return ShipScene")
        //            return
        //        }
        
        //        for child in shipScene.rootNode.childNodes {
        //            child.geometry?.firstMaterial?.lightingModel = .physicallyBased
        //            child.movabilityHint = .movable
        //            wrapperNode.addChildNode(child)
        //        }
        //
        plane.materials.first?.diffuse.contents = UIColor.clear
        
        
        // shipScene.background.contents = UIImage(named: "poster-1")
        
        wrapperNode.position = SCNVector3(x,y,z)
        isModelAdded = true
        sceneView.scene.rootNode.addChildNode(wrapperNode)
    }
    
    func addTapGestureToSceneView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GARSurfaceDetectViewController.addShipToSceneView(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
        
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinch(_:)))
        sceneView.addGestureRecognizer(pinchGestureRecognizer)
    }
    
    @objc func pinch(_ pinch: UIPinchGestureRecognizer) {
        
        switch pinch.state {
        case .began: fallthrough
        case .changed: wrapperNode.scale = SCNVector3(pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale), pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale), pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale))
        case .ended:
            wrapperNode.scale = SCNVector3(pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale), pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale), pinch.scale > 3 ? 3 : (pinch.scale < 0.1 ? 0.1 : pinch.scale))
        default: break
        }
        
    }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "AR"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.isHidden = false
        
        let sideMenu = UIBarButtonItem(image: UIImage(named: "previous") , style: .done, target: self, action: #selector(btnMenuClicked))
      // let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
     //   self.navigationItem.rightBarButtonItem = cartMenu
    }
    //MARK: Button Events
    @objc func btnMenuClicked()
    {
        self.navigationController?.popViewController(animated: true)
       // showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
        goToCart()
    }
    func goToCart()
    {
        
        let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        myBagVC.isDirect = true
        self.navigationController?.pushViewController(myBagVC, animated: true)
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension UIColor {
    open class var transparentLightBlue: UIColor {
        return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.50)
    }
}
//extension GARVerticalARSurfaceViewController: ARSCNViewDelegate {
//
//
//
//
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//        // 1
//        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
//
//        // 2
//        let width = CGFloat(planeAnchor.extent.x)
//        let height = CGFloat(planeAnchor.extent.z)
//        plane = SCNPlane(width: width, height: height)
//
//        // 3
//
//
//        plane.materials.first?.diffuse.contents = UIImage(named: "poster-1")
//        //plane.cornerRadius = plane.height/2 //UNComment This
//        // 4
//
//        if(!isModelAdded)
//        {
//            let planeNode = SCNNode(geometry: plane)
//
//            // 5
//            let x = CGFloat(planeAnchor.center.x)
//            let y = CGFloat(planeAnchor.center.y)
//            let z = CGFloat(planeAnchor.center.z)
//            planeNode.position = SCNVector3(x,y,z)
//            planeNode.eulerAngles.x = -.pi / 2
//
//            // 6
//
//
//            node.addChildNode(planeNode)
//        }
//    }
//
//    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
//        // 1
//        guard let planeAnchor = anchor as?  ARPlaneAnchor,
//            let planeNode = node.childNodes.first,
//            let plane = planeNode.geometry as? SCNPlane
//            else { return }
//
//        // 2
//        let width = CGFloat(planeAnchor.extent.x)
//        let height = CGFloat(planeAnchor.extent.z)
//        plane.width = width
//        plane.height = height
//
//        // 3
//        let x = CGFloat(planeAnchor.center.x)
//        let y = CGFloat(planeAnchor.center.y)
//        let z = CGFloat(planeAnchor.center.z)
//        planeNode.position = SCNVector3(x, y, z)
//
//    }
//}


extension GARVerticalARSurfaceViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // 1
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        // 2
        //let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)


        let image = selectedImage
        // 3
       // plane.materials.first?.diffuse.contents = #imageLiteral(resourceName: "poster")

//        let image = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
//        image.contentMode = .scaleAspectFit
//        image.image = #imageLiteral(resourceName: "poster")

        let ratio = (image?.size.width ?? 0.0) / (image?.size.height ?? 1.0)
        let plane = SCNPlane(width: height*ratio, height: height)

        plane.materials.first?.diffuse.contents = image
        // 4
        let planeNode = SCNNode(geometry: plane)

        // 5
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2

        // 6
        node.addChildNode(planeNode)
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // 1
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }

        // 2
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        
        if let image = selectedImage
        {
            let ratio = image.size.width/image.size.height
            plane.width = height*ratio
            plane.height = height
        }
        // 3
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x, y, z)
    }
}
