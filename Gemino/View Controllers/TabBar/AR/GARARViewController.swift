//
//  GARARViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 28/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import MobileBuySDK
import SDWebImage
import InteractiveSideMenu
import SceneKit

class GARARViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, SideMenuItemContent {
    
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var colCollections: UICollectionView!
    @IBOutlet weak var colProducts: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnStack: UIStackView!
    @IBOutlet weak var ModelScene: SCNView!
    
    
    var collectionNodes = [Storefront.CollectionEdge]()
    var imageView : UIImageView!
    var selectedBottonIndex : Int = -1
    //var currentProducts = [GARProduct]()
    var currentCollection : Storefront.CollectionEdge!
    var currentProductCollection : Storefront.ProductEdge!
    var currentProduct : Storefront.ProductVariantEdge!
    var selectedImage : UIImage = #imageLiteral(resourceName: "test")
    var currentProductID : String = ""
    var isDirect : Bool = false
    var isFront : Bool = false
    let wrapperNode = SCNNode()

    
    //var selectedProduct : GARProduct!
    
    // MARK: Session Management
    
   
    let faceDetector = GARFaceLandmarksDetector()
    var captureSession = AVCaptureSession()
    @IBOutlet weak var image_View: UIImageView!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDevice()
        
//        if(self.selectedImage != nil)
//        {
//            self.previewView.addGlassOnFace(face: face, image: GARHelper().removeBackground(image: self.selectedImage))
//            self.previewView.drawFaceWithLandmarks(face: face)
//        }
        
        fetchData(collectionName: "Mens eyewear")
        fetchData(collectionName: "Womens eyewear")
        fetchData(collectionName: "Hats")
        fetchData(collectionName: "Mens watches")
        fetchData(collectionName: "Womens watches")
        prepareView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        addSample()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    func addSample(){
        let shipScene = SCNScene(named: "robotHead.scn", inDirectory: "Models.scnassets")
        if(shipScene != nil)
        {
            for child in (shipScene?.rootNode.childNodes)! {
                child.geometry?.firstMaterial?.lightingModel = .physicallyBased
                child.movabilityHint = .movable
                self.wrapperNode.addChildNode(child)
            }
            
            self.wrapperNode.position = SCNVector3(0, -2, -2)
            ModelScene.scene?.rootNode.addChildNode(self.wrapperNode)
        }
    }
    
    
    
    //MARK: Basic Functions
    private func getDevice() -> AVCaptureDevice? {
        if(isFront)
        {
        let discoverSession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInTelephotoCamera, .builtInWideAngleCamera], mediaType: .video, position: .front)
        return discoverSession.devices.first
        }
        let discoverSession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInTelephotoCamera, .builtInDualCamera], mediaType: .video, position: .back)
        return discoverSession.devices.first
    }
    
    private func configureDevice() {
        if let device = getDevice() {
            do {
                try device.lockForConfiguration()
                if device.isFocusModeSupported(.continuousAutoFocus) {
                    device.focusMode = .continuousAutoFocus
                }
                device.unlockForConfiguration()
            } catch { print("failed to lock config") }
            
            do {
                let input = try AVCaptureDeviceInput(device: device)
                captureSession.addInput(input)
            } catch { print("failed to create AVCaptureDeviceInput") }
            
            captureSession.startRunning()
            
            let videoOutput = AVCaptureVideoDataOutput()
            videoOutput.videoSettings = [String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_32BGRA)]
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .utility))
            
            if captureSession.canAddOutput(videoOutput) {
                captureSession.addOutput(videoOutput)
            }
        }
    }
    
    
    func prepareView()
    {
        //colCollections.isHidden = true
        btnBack.isHidden = !isDirect
        btnStack.isHidden = true
        GARHelper().roundCorner(view: btnCheckOut, corner: btnCheckOut.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: btnAddToCart, corner: btnAddToCart.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        
        colCollections.delegate = self
        colCollections.dataSource = self
        colCollections.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
        
        colProducts.delegate = self
        colProducts.dataSource = self
        colProducts.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
        
        // Set up the video preview view.
        
        
        // Set up Vision Request
        
    }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "AR"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.isHidden = false
        
        let sideMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-2") , style: .done, target: self, action: #selector(btnMenuClicked))
        let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
        self.navigationItem.rightBarButtonItem = cartMenu
    }
    
    
    func goToCart()
    {
        
        let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        myBagVC.isDirect = true
        self.navigationController?.pushViewController(myBagVC, animated: true)
    }
    
    //MARK: Button Events
    @objc func btnMenuClicked()
    {
        showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
        goToCart()
    }
    
    @IBAction func btnSwapClicked(_ sender: UIButton) {
        isFront = !isFront
        for input in captureSession.inputs
        {
            captureSession.removeInput(input)
        }
        for output in captureSession.outputs
        {
            captureSession.removeOutput(output)
        }
        configureDevice()
    }
    
    
//    Abstract
//
//    Contemporary
//
//    Landscapes
//
//    Motivation
//    
//    Music
//
//    Pop Art
//
//    Space
//
//    Vehicles
//
//    Wildlife
//
//    Trending Now
    
    //MARK: CollectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(currentCollection != nil)
        {
            if(currentProductCollection != nil)
            {
                if(indexPath.row == 0)
                {
                    currentProductCollection = nil
                    currentProduct = nil
                }
                else
                {
                    let cell = collectionView.cellForItem(at: indexPath) as! GARCircleCollectionCell
                    selectedImage = cell.imgCollection.image!
                    currentProduct = currentProductCollection.node.variants.edges[indexPath.row - 1]
                }
            }
            else
            {
                if(indexPath.row == 0)
                {
                    btnStack.isHidden = true
                    currentCollection = nil
                }
                else
                {
                    currentProductCollection = currentCollection.node.products.edges[indexPath.row - 1]
                }
            }
        }
        else
        {
            btnStack.isHidden = false
            currentCollection = collectionNodes[indexPath.row]
        }
        colProducts.reloadData()
        colCollections.reloadData()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.contentInset = UIEdgeInsetsMake(0, 30, 0, 30)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARCircleCollectionCell", for: indexPath) as! GARCircleCollectionCell
        GARHelper().roundCorner(view: cell.imgCollection, corner: cell.imgCollection.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        
        
        
        if(currentCollection != nil)
        {
            if(indexPath.row == 0)
            {
                cell.imgCollection.backgroundColor = UIColor.lightGray
                cell.imgCollection.image = #imageLiteral(resourceName: "ic_Logo")
                cell.lblCollection.text = "Back"
                return cell
            }
            
            if(currentProductCollection != nil)
            {
                //cell.imgCollection.image = #imageLiteral(resourceName: "ic_Logo")
                
                
                cell.imgCollection.sd_setImage(with: currentProductCollection.node.variants.edges[indexPath.row - 1].node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
               // cell.imgCollection.sd_setImage(with: currentProductCollection.node.collections.edges[indexPath.row].node.image?.originalSrc, completed: nil)
                cell.lblCollection.text = currentProductCollection.node.variants.edges[indexPath.row - 1].node.title
            }
            else
            {
                //cell.imgCollection.image = #imageLiteral(resourceName: "ic_Logo")
                cell.imgCollection.sd_setImage(with: currentCollection.node.products.edges[indexPath.row - 1].node.variants.edges.first?.node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
                
                //cell.imgCollection.sd_setImage(with: currentCollection.node.products.edges[indexPath.row].node.collections.edges.first?.node.image?.originalSrc, completed: nil)
                cell.lblCollection.text = currentCollection.node.products.edges[indexPath.row - 1].node.title
            }
        }
        else
        {
          //  cell.imgCollection.image = #imageLiteral(resourceName: "ic_Logo")
            
            cell.imgCollection.sd_setImage(with: collectionNodes[indexPath.row].node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
      //      cell.imgCollection.sd_setImage(with: collectionNodes[indexPath.row].node.image?.originalSrc, completed: nil)
            cell.lblCollection.text = collectionNodes[indexPath.row].node.title
        }
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(currentCollection != nil)
        {
            if(currentProductCollection != nil)
            {
                return currentProductCollection.node.variants.edges.count + 1
            }
            return currentCollection.node.products.edges.count + 1
        }
        return collectionNodes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 110)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
//
    
    //MARK: Button Events
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnAddToCartClicked(_ sender: UIButton) {
        GARHelper().createAlert(message: "Successfully Added to Cart", VC: self)
        if(currentProduct != nil)
        {
            cartProducts.append(currentProduct)
        }
//        if(currentProduct != nil)
//        {
//        currentProducts.append(currentProduct)
//        }
    }
    
    
    @IBAction func btnGoToCheckOutClicked(_ sender: UIButton) {
        
        let controller = storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        controller.isDirect = true
        self.navigationController?.pushViewController(controller, animated: true)
        
      //  self.navigationController?.present(controller, animated: true, completion: nil)
    
    }
    
    
    
    //MARK: Fetch Data
    func fetchData(collectionName : String)
    {
        
        let client = Graph.Client(
            shopDomain: shopingDomain,
            apiKey:     API
        )
        
        // client.
        
        
        
        let query = Storefront.buildQuery { $0
            .shop { $0
                .name()
                
                .collections(first: 5, query: collectionName,{$0
                    .edges { $0
                        .node { $0
                            .id()
                            .title()
                            .description()
                            .image({ $0
                                .originalSrc()
                                .id()
                                .altText()
                                .transformedSrc()
                            })
                            .products(first: 10, { $0
                                .edges({ $0
                                    .node({ $0
                                        .id()
                                        .title()
                                        .description()
                                        .vendor()
                                        .variants(first: 5,{ $0
                                            .edges({ $0
                                                .node({ $0
                                                    .id()
                                                    .title()
                                                    
                                                    .image({ $0
                                                    .originalSrc()
                                                    .id()
                                                    .altText()
                                                    .transformedSrc()
                                                     })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        }
                    }
                })
                
                
                
                
                
//                .collections(first: 10, query: "", { $0
//                    .edges { $0
//                        .node { $0
//                            .id()
//                            .title()
//                            .description()
//                            .image({ $0
//                                .originalSrc()
//                                .id()
//                                .altText()
//                                .transformedSrc()
//                            })
//
////                            .products(first: 10, { $0
////                                .edges({ $0
////                                    .node({ $0
////                                        .id()
////                                        .title()
////                                        .description()
////                                        .vendor()
////                                        .collections(first: 4, { $0
////                                            .edges({ $0
////                                                .node({ $0
////                                                    .id()
////                                                    .title()
////                                                    .description()
////                                                    .image({ $0
////                                                        .originalSrc()
////                                                        .id()
////                                                        .altText()
////                                                        .transformedSrc()
////                                                    })
////
////                                                })
////
////                                            })
////                                        })
////
////                                    })
////                                })
////                            })
//                        }
//                    }
//                })
            }
        }
       
        let task = client.queryGraphWith(query) { response, error in
            if let response = response {
                //print(response.nodes)
                let name = response.shop.name
                self.collectionNodes.append(contentsOf: response.shop.collections.edges)
               // self.collectionNodes = response.shop.collections.edges
                DispatchQueue.main.async {
                    self.colCollections.reloadData()
                    self.colProducts.reloadData()
                }
                print(name)
            } else {
                print("Query failed: \(String(describing: error))")
            }
        }
        task.resume()
        
      
    }
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}
extension GARARViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        // Scale image to process it faster
        let maxSize = CGSize(width: 1024, height: 1024)
        if(isFront)
        {
            if let image = UIImage(sampleBuffer: sampleBuffer)?.flipped()?.imageWithAspectFit(size: maxSize) {
                
                faceDetector.highlightFaces(for: image, glassImage: GARHelper().removeBackground(image: self.selectedImage), scene: ModelScene) { (resultImage) in
                    DispatchQueue.main.async {
                        self.image_View?.image = resultImage
                    }
                }
            }
        }
        else
        {
            if let image = UIImage(sampleBuffer: sampleBuffer)?.imageWithAspectFit(size: maxSize) {
                
                faceDetector.highlightFaces(for: image, glassImage: GARHelper().removeBackground(image: self.selectedImage), scene: ModelScene) { (resultImage) in
                    DispatchQueue.main.async {
                        self.image_View?.image = resultImage
                    }
                }
            }
        }
    }
}
