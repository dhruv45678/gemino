//
//  GARProfileViewController.swift
//  Gemino
//
//  Created by Khatri Dhruv on 19/07/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu
class GARProfileViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SideMenuItemContent {

    @IBOutlet weak var colYourOrders: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Basic Functions
    func prepareView()
    {
        colYourOrders.delegate = self
        colYourOrders.dataSource = self
        colYourOrders.register(UINib(nibName: "GARMYBagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GARMYBagCollectionViewCell")
       
    }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "PROFILE"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.isHidden = false
        
        let sideMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-2") , style: .done, target: self, action: #selector(btnMenuClicked))
        let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
        self.navigationItem.rightBarButtonItem = cartMenu
    }
    
    func goToCart()
    {
        
        let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        myBagVC.isDirect = true
        self.navigationController?.pushViewController(myBagVC, animated: true)
    }
    
    //MARK: Button Events
    @objc func btnMenuClicked()
    {
        showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
        goToCart()
    }
    
    
    
    //MARK: CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARMYBagCollectionViewCell", for: indexPath) as! GARMYBagCollectionViewCell
        cell.imgProduct.image = #imageLiteral(resourceName: "ic_Logo")
        cell.lblName.text = "iPhone X"
        cell.btnPlus.isHidden = true
        cell.btnMinus.isHidden = true
        cell.viewSaperator.isHidden = true
        
        //cell.lblPrice.text = "\(cartProducts[indexPath.row].node.price)"
        return cell
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 120)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
