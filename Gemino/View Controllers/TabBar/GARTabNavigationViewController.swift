//
//  GARTabNavigationViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 18/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARTabNavigationViewController: UINavigationController, SideMenuItemContent {

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Basic Functions
    func prepareView()
    {
        self.navigationController?.navigationBar.topItem?.title = "GEMINO"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
        let sideMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-2") , style: .done, target: self, action: #selector(btnMenuClicked))
        let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        //  let centerLogo = UIBarButtonItem(title: "GEMINO", style: .done, target: self, action: #selector(btnLogoClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
        self.navigationItem.rightBarButtonItem = cartMenu
    }
    
    func goToCart()
    {
        
        let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        myBagVC.isDirect = true
        self.navigationController?.pushViewController(myBagVC, animated: true)
        
//        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARMyBagViewController") as? GARMyBagViewController {
//            viewController.isDirect = true
//            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
//            navigationController.pushViewController(viewController, animated: true)
//        }
    }
    
    //MARK: Button Events
    @objc func btnMenuClicked()
    {
        showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
        goToCart()
    }
    
    @objc func btnLogoClicked()
    {
        print("Logo")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
