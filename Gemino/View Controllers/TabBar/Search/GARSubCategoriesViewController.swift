//
//  GARSubCategoriesViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 26/07/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARSubCategoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblSubCategories: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        previewView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Basic Functions
    func previewView()
    {
        tblSubCategories.dataSource = self
        tblSubCategories.delegate = self
        tblSubCategories.register(UINib(nibName: "GARMenuCell", bundle: nil), forCellReuseIdentifier: "GARMenuCell")
    }
    
    //MARK: Button Events
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Table Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GARMenuCell", for: indexPath) as! GARMenuCell
        cell.imgMenu.image = #imageLiteral(resourceName: "ic_Logo")
        GARHelper().roundCorner(view: cell.imgMenu, corner: cell.imgMenu.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        cell.lblTitle.text = "Trending..."
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
