//
//  GARHomeViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 07/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import Firebase
import InteractiveSideMenu
import Vision
import MobileBuySDK
import SDWebImage
import SceneKit
class GARHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SideMenuItemContent, Storyboardable {
    
    

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblHome: UITableView!
    //var arrHome = ["Collection" , "Big Banner", "New Arrival", "Discount"]
    //var arrHome = ["Small Banner", "Medium Banner", "Big Banner", "Animated Product" ,"Simple Product", "Circle"]
    
    //var arrHome = ["SmallBanner", "Circle", "SmallBanner" ,"AnimatedProduct" ,"MediumBanner" ,"SimpleProduct" ,"BigBanner" ,"Grid", "SmallBanner"]
    var abstractNodes = [Storefront.CollectionEdge]()
    var contemporaryNodes = [Storefront.CollectionEdge]()
    var landscapesNodes = [Storefront.CollectionEdge]()
    var motivationNodes = [Storefront.CollectionEdge]()
    var musicNodes = [Storefront.CollectionEdge]()
    var popArtNodes = [Storefront.CollectionEdge]()
    var spaceNodes = [Storefront.CollectionEdge]()
    var vehiclesNodes = [Storefront.CollectionEdge]()
    var wildLifeNodes = [Storefront.CollectionEdge]()
    var trendingNodes = [Storefront.CollectionEdge]()
    
    var collections = [Storefront.CollectionEdge]()
    
    var arrProduct = ["SmallBanner" : "Ad", "MediumBanner" : "Coupon", "BigBanner" : "Offers", "AnimatedProduct" : "New Arrival" ,"SimpleProduct" : "Discount", "Circle" : "Collection"]
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tblHome.reloadData()
    }
    
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch arrHome[indexPath.row] {
        case "SmallBanner":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARSmallBannerCell", for: indexPath) as! GARSmallBannerCell
            
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = true
            cell.reloadData(banner: self.abstractNodes)
            
            return cell
            
        case "MediumBanner":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARMediumBannerCell", for: indexPath) as! GARMediumBannerCell
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = true
            cell.reloadData(banner: self.contemporaryNodes)
            return cell
        case "BigBanner":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARBigBannerCell", for: indexPath) as! GARBigBannerCell
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = true
            cell.reloadData(banner: self.landscapesNodes)
            return cell
        case "AnimatedProduct":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARProductCell", for: indexPath) as! GARProductCell
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = false
            cell.reloadData(banner: self.musicNodes)
            return cell
        case "SimpleProduct":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARNormalProductCell", for: indexPath) as! GARNormalProductCell
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = false
            cell.reloadData(banner: self.wildLifeNodes)
            return cell
        case "Circle":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARCollectionCell", for: indexPath) as! GARCollectionCell
            cell.colCell.isPagingEnabled = false
            cell.colCell.tag = indexPath.row
            cell.reloadData(banner: self.collections)
            return cell
        case "Grid":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARGridProductCell", for: indexPath) as! GARGridProductCell
            cell.colCell.tag = indexPath.row
            cell.colCell.isPagingEnabled = false
            cell.reloadData(banner: self.trendingNodes)
            return cell
        case "AR":
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARARCell", for: indexPath) as! GARARCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "GARCollectionCell", for: indexPath) as! GARCollectionCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHome.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch arrHome[indexPath.row] {
        case "SmallBanner":
            return 112
        case "MediumBanner":
            return 212
        case "BigBanner":
            return 312
        case "AnimatedProduct":
            return 400
        case "SimpleProduct":
            return 400
        case "Circle":
            return 140
        case "Grid":
            return 400
        case "AR":
            return 600
        default:
            return 0
        
        }
    }
    
    
    
    //MARK: Basic Functions
    func prepareView()
    {
        
        
   //     GARHelper().addTItle(view: self.view, value: "GARHomeViewController")
        
        fetchData(collectionName: "Abstract")
        fetchData(collectionName: "Contemporary")
        fetchData(collectionName: "Landscapes")
        fetchData(collectionName: "Motivation")
        fetchData(collectionName: "Music")
        fetchData(collectionName: "Pop Art")
        fetchData(collectionName: "Space")
        fetchData(collectionName: "Vehicles")
        fetchData(collectionName: "Wildlife")
        fetchData(collectionName: "Trending Now")
        fetchCollections()
//        fetchData(collectionName: "Mens eyewear")
//        fetchData(collectionName: "Womens eyewear")
//        fetchData(collectionName: "Hats")
//        fetchData(collectionName: "Mens watches")
//        fetchData(collectionName: "Womens watches")
        
        
        GARHelper().roundCorner(view: txtSearch, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        tblHome.delegate = self
        tblHome.dataSource = self
        tblHome.register(UINib(nibName: "GARCollectionCell", bundle: nil), forCellReuseIdentifier: "GARCollectionCell")
        tblHome.register(UINib(nibName: "GARProductCell", bundle: nil), forCellReuseIdentifier: "GARProductCell")
        tblHome.register(UINib(nibName: "GARSmallBannerCell", bundle: nil), forCellReuseIdentifier: "GARSmallBannerCell")
        tblHome.register(UINib(nibName: "GARMediumBannerCell", bundle: nil), forCellReuseIdentifier: "GARMediumBannerCell")
        tblHome.register(UINib(nibName: "GARBigBannerCell", bundle: nil), forCellReuseIdentifier: "GARBigBannerCell")
        tblHome.register(UINib(nibName: "GARNormalProductCell", bundle: nil), forCellReuseIdentifier: "GARNormalProductCell")
        tblHome.register(UINib(nibName: "GARGridProductCell", bundle: nil), forCellReuseIdentifier: "GARGridProductCell")
        tblHome.register(UINib(nibName: "GARARCell", bundle: nil), forCellReuseIdentifier: "GARARCell")
        
        
        }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "GEMINO"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.isHidden = false
        
        let sideMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-2") , style: .done, target: self, action: #selector(btnMenuClicked))
        let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
        self.navigationItem.rightBarButtonItem = cartMenu
    }
    
    
        func goToCart()
        {
            
            let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
            myBagVC.isDirect = true
            self.navigationController?.pushViewController(myBagVC, animated: true)
        }
        
        //MARK: Button Events
        @objc func btnMenuClicked()
        {
            showSideMenu()
        }
        
        @objc func btnCartClicked()
        {
            print("Cart")
            goToCart()
        }
   
    public func moveToDetails()
    {
        let controller = storyboard?.instantiateViewController(withIdentifier: "GARProductDetailsPageViewController") as! GARProductDetailsPageViewController
        //viewController.product = banners[0].node.products.edges[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //MARK: Function Events
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        
    }
    
    
    
    //MARK: Fetch Data
    func fetchData(collectionName : String)
    {
        
        
        
        
        
        let client = Graph.Client(
            shopDomain: shopingDomain,
            apiKey:     API
        )
        
       
        
        
//        let query = Storefront.buildQuery { $0
//            .shop { $0
//                .collections(first: 10, query: collectionName) { $0
//                    .edges { $0
//                        .node { $0
//                            .id()
//                            .title()
//                            .description()
//                        }
//                    }
//                }
//            }
//        }
        
        
        // client.
        let query = Storefront.buildQuery { $0
            .shop { $0
                .name()
                .collections(first: 1, query: collectionName,{$0
                    .edges { $0
                        .node { $0
                            .id()
                            .title()
                            .description()
                            .image({ $0
                                .originalSrc()
                                .id()
                                .altText()
                                .transformedSrc()
                            })
                            
                            .products(first: 40, { $0
                                
                                .edges({ $0
                                    
                                    .node({ $0
//                                        .variants({$0
//                                            .edges({ $0
//                                                .node({ $0
//                                                    .id()
//                                                    .title()
//                                                    .image({ $0
//                                                        .originalSrc()
//                                                        .id()
//                                                        .altText()
//                                                        .transformedSrc()
//                                                    })
//                                                })
//                                            })
//                                        })
                                        
                                        .id()
                                        .title()
                                        .description()
                                    
                                        .priceRange({$0
                                            .maxVariantPrice({$0
                                                .amount()
                                                .currencyCode()
                                            })
                                        })
    
                                        .vendor()
                                        .images(first: 10,{$0
                                            .edges({$0
                                                .node({$0
                                                    .transformedSrc()
                                                    .originalSrc()
                                                    .id()
                                                    .altText()
                                                })
                                            })
                                        })
//                                        .variants(first: 5,{ $0
//                                            .edges({ $0
//                                                .node({ $0
//                                                    .id()
//                                                    .title()
//                                                    .image({ $0
//                                                        .originalSrc()
//                                                        .id()
//                                                        .altText()
//                                                        .transformedSrc()
//                                                    })
//                                                })
//                                            })
//                                        })
                                    })
                                })
                            })
                        }
                    }
                })
            }
        }
        
        
        
        let task = client.queryGraphWith(query) { response, error in
            if let response = response {
                //print(response.nodes)
                let name = response.shop.name
                self.setValues(collectionName: collectionName, collectionNode: response.shop.collections.edges)
                print(name)
            } else {
                print("Query failed: \(String(describing: error))")
            }
        }
        task.resume()
    }
    
    
    func fetchCollections()
    {
        let client = Graph.Client(
            shopDomain: shopingDomain,
            apiKey:     API
        )
        // client.
        let query = Storefront.buildQuery { $0
            .shop { $0
                .name()
                .collections(first: 20, {$0
                    .edges { $0
                        .node { $0
                            .id()
                            .title()
                            .description()
                            .image({ $0
                                .originalSrc()
                                .id()
                                .altText()
                                .transformedSrc()
                            })
                            .products(first: 40, { $0
                                .edges({ $0
                                    .node({ $0
                                        .id()
                                        .title()
                                        .description()
//                                        .priceRange(alias: "Price", {$0
//                                            .maxVariantPrice({$0
//                                                .amount()
//                                                .currencyCode()
//                                            })
//                                        })
                                        
                                        .vendor()
                                        .images(first: 10,{$0
                                            .edges({$0
                                                .node({$0
                                                    .transformedSrc()
                                                    .originalSrc()
                                                    .id()
                                                    .altText()
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        }
                    }
                })
            }
        }
        
        
        
        let task = client.queryGraphWith(query) { response, error in
            if let response = response {
                //print(response.nodes)
                self.collections = response.shop.collections.edges
                self.tblHome.reloadData()
            } else {
                print("Query failed: \(String(describing: error))")
            }
        }
        task.resume()
    }
    
    
    
    func setValues(collectionName : String, collectionNode : [Storefront.CollectionEdge])
    {
        switch collectionName {
        case "Abstract":
            self.abstractNodes.append(contentsOf: collectionNode)
            break
        case "Contemporary":
            self.contemporaryNodes.append(contentsOf: collectionNode)
            break
        case "Landscapes":
            self.landscapesNodes.append(contentsOf: collectionNode)
            break
        case "Motivation":
            self.motivationNodes.append(contentsOf: collectionNode)
            break
        case "Music":
            self.musicNodes.append(contentsOf: collectionNode)
            break
        case "Pop Art":
            self.popArtNodes.append(contentsOf: collectionNode)
            break
        case "Space":
            self.spaceNodes.append(contentsOf: collectionNode)
            break
        case "Vehicles":
            self.vehiclesNodes.append(contentsOf: collectionNode)
            break
        case "Wildlife":
            self.wildLifeNodes.append(contentsOf: collectionNode)
            break
        case "Trending Now":
            self.trendingNodes.append(contentsOf: collectionNode)
            break
        default:
            break
        }
        
  //      self.collectionNode.append(contentsOf: response.shop.collections.edges)
        // self.collectionNodes = response.shop.collections.edges
        DispatchQueue.main.async {
            self.tblHome.reloadData()
            //self.colCollections.reloadData()
           // self.colProducts.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

