//
//  GARMyBagViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu
import MobileBuySDK
import SafariServices

class GARMyBagViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SideMenuItemContent {

    @IBOutlet weak var colMyBag: UICollectionView!
   // @IBOutlet weak var btnSideMenu: UIView!
    @IBOutlet weak var viewMyBag: UIView!
    
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var btnApplePay: UIButton!
    
    var isDirect : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: Basic Functions
    func prepareView()
    {
        GARHelper().roundCorner(view: btnCheckOut, corner: btnCheckOut.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: btnApplePay, corner: btnApplePay.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        colMyBag.delegate = self
        colMyBag.dataSource = self
        colMyBag.register(UINib(nibName: "GARMYBagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GARMYBagCollectionViewCell")
    }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = true
        var image = UIImage()
        if(isDirect)
        {
            viewMyBag.isHidden = false
            image = #imageLiteral(resourceName: "previous")
        }
        else
        {
            viewMyBag.isHidden = false
            image = #imageLiteral(resourceName: "menu-2")
            
        }
        btnSideMenu.setImage(image, for: .normal)

    }
    
    //MARK: CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARMYBagCollectionViewCell", for: indexPath) as! GARMYBagCollectionViewCell
        cell.imgProduct.sd_setImage(with: cartProducts[indexPath.row].node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
        cell.lblName.text = cartProducts[indexPath.row].node.title
        //cell.lblPrice.text = "\(cartProducts[indexPath.row].node.price)"
        return cell
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cartProducts.count
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 180)
    }
    
    
    //MARK: Button Events
    
    @objc func btnMenuClicked()
    {
        if(isDirect)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            showSideMenu()
        }
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
    }
    
    
    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        if(isDirect)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            showSideMenu()
        }
    }
    @IBAction func btnCheckOutClicked(_ sender: UIButton) {
       // GARHelper().createAlert(message: "Your Order is successfully Placed.", VC: self)
        createCheckOut()
    }
    
    @IBAction func btnApplePayClicked(_ sender: UIButton) {
        GARHelper().createAlert(message: "Your Order is successfully Placed Using Apple Pay.", VC: self)
    }
    
    
    //MARK: Checkout Function
    func createCheckOut()
    {
        var checkoutProducts = [Storefront.CheckoutLineItemInput]()
        for product in cartProducts
        {
            checkoutProducts.append(Storefront.CheckoutLineItemInput.create(quantity: 1, variantId: product.node.id))
        }
        
        
        let input = Storefront.CheckoutCreateInput.create(
            lineItems: .value(checkoutProducts)
        )
        
        let client = Graph.Client(
            shopDomain: shopingDomain,
            apiKey:     API
        )
        
        let mutation = Storefront.buildMutation { $0
            .checkoutCreate(input: input) { $0
                .checkout { $0
                    .id()
                }
                .userErrors { $0
                    .field()
                    .message()
                }
            }
        }
        
        let task = client.mutateGraphWith(mutation) { result, error in
            guard error == nil else {
                // handle request error
                print(error?.localizedDescription ?? "")
                return
            }
            
            guard let userError = result?.checkoutCreate?.userErrors else {
                // handle any user error
                return
            }
             print(userError)
            let checkoutID = result?.checkoutCreate?.checkout?.id
            let mutation = Storefront.buildMutation { $0
                .checkoutEmailUpdate(checkoutId: checkoutID!, email: "khatridhruv4567@gmail.com") { $0
                    .checkout { $0
                        .id()
                    }
                    .userErrors { $0
                        .field()
                        .message()
                    }
                }
            }
            
            let retry = Graph.RetryHandler<Storefront.QueryRoot>(endurance: .finite(10)) { (response, error) -> Bool in
                return (response?.node as? Storefront.Checkout)?.availableShippingRates?.ready ?? false == false
            }
            
            let query = Storefront.buildQuery { $0
                .node(id: checkoutID!) { $0
                    .onCheckout { $0
                        .id()
                        .availableShippingRates { $0
                            .ready()
                            .shippingRates { $0
                                .handle()
                                .price()
                                .title()
                            }
                        }
                    }
                }
            }
            
            let task = client.queryGraphWith(query, retryHandler: retry) { response, error in
                let checkout      = (response?.node as? Storefront.Checkout)
                self.openSafariFor(checkout!)
               // let shippingRates = checkout.availableShippingRates?.shippingRates
            }
            task.resume()
            
            
            
            
//            let checkoutID = result?.checkoutCreate?.checkout?.id
//            result?.checkoutCreate?.checkout
//            result?.checkoutCreate?.checkout?.webUrl
        }
        task.resume()
        
        
        //Storefront.Checkout
        
        
    }
    
    
    
    
    
    //MARK: Extra Checkout Part
    
    func openSafariFor(_ checkout: Storefront.Checkout) {
        let safari                  = SFSafariViewController(url: checkout.webUrl)
        safari.navigationItem.title = "Checkout"
        self.navigationController?.present(safari, animated: true, completion: nil)
    }
    
    func authorizePaymentFor(_ shopName: String, in checkout: Storefront.Checkout) {
        let payCurrency = PayCurrency(currencyCode: "CAD", countryCode: "CA")
        let payItems = checkout.lineItems.edges.map { item in
            PayLineItem(price: (item.node.variant?.price)!, quantity: Int(item.node.quantity))
        }
//        let payItems    = checkout.lineItems.map { item in
//            PayLineItem(price: item.individualPrice, quantity: item.quantity)
//        }
        
        let payCheckout = PayCheckout(
            id:              checkout.id.rawValue,
            lineItems:       payItems,
            discount:        nil,
            shippingAddress: nil,
            shippingRate:    nil,
            subtotalPrice:   checkout.subtotalPrice,
            needsShipping:   checkout.requiresShipping,
            totalTax:        checkout.totalTax,
            paymentDue:      checkout.paymentDue
        )
        
//        let paySession      = PaySession(shopName: shopName, checkout: payCheckout, currency: payCurrency, merchantID: Client.merchantID)
//        paySession.delegate = self
//        self.paySession     = paySession
//
//        paySession.authorize()
    }
    
    // ----------------------------------
    //  MARK: - Discount Codes -
    //
    func promptForDiscountCode(completion: @escaping (String?) -> Void) {
        let alert = UIAlertController(title: "Do you have a discount code?", message: "Any valid discount code can be applied to your checkout.", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.attributedPlaceholder = NSAttributedString(string: "Discount code")
        }
        
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
            completion(nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes, apply code", style: .cancel, handler: { [unowned alert] action in
            let code = alert.textFields!.first!.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if let code = code, code.count > 0 {
                completion(code)
            } else {
                completion(nil)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
   
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension GARMyBagViewController: PaySessionDelegate {
//
//    func paySession(_ paySession: PaySession, didRequestShippingRatesFor address: PayPostalAddress, checkout: PayCheckout, provide: @escaping  (PayCheckout?, [PayShippingRate]) -> Void) {
//
//        print("Updating checkout with address...")
//        Client.shared.updateCheckout(checkout.id, updatingPartialShippingAddress: address) { checkout in
//
//            guard let checkout = checkout else {
//                print("Update for checkout failed.")
//                provide(nil, [])
//                return
//            }
//
//            print("Getting shipping rates...")
//            Storefront.Client.shared.fetchShippingRatesForCheckout(checkout.id) { result in
//                if let result = result {
//                    print("Fetched shipping rates.")
//                    provide(result.checkout.payCheckout, result.rates.payShippingRates)
//                } else {
//                    provide(nil, [])
//                }
//            }
//        }
//    }
//
//    func paySession(_ paySession: PaySession, didUpdateShippingAddress address: PayPostalAddress, checkout: PayCheckout, provide: @escaping (PayCheckout?) -> Void) {
//
//        print("Updating checkout with shipping address for tax estimate...")
//        Client.shared.updateCheckout(checkout.id, updatingPartialShippingAddress: address) { checkout in
//
//            if let checkout = checkout {
//                provide(checkout.payCheckout)
//            } else {
//                print("Update for checkout failed.")
//                provide(nil)
//            }
//        }
//    }
//
//    func paySession(_ paySession: PaySession, didSelectShippingRate shippingRate: PayShippingRate, checkout: PayCheckout, provide: @escaping  (PayCheckout?) -> Void) {
//
//        print("Selecting shipping rate...")
//        Client.shared.updateCheckout(checkout.id, updatingShippingRate: shippingRate) { updatedCheckout in
//            print("Selected shipping rate.")
//            provide(updatedCheckout?.payCheckout)
//        }
//    }
//
//    func paySession(_ paySession: PaySession, didAuthorizePayment authorization: PayAuthorization, checkout: PayCheckout, completeTransaction: @escaping (PaySession.TransactionStatus) -> Void) {
//
//        guard let email = authorization.shippingAddress.email else {
//            print("Unable to update checkout email. Aborting transaction.")
//            completeTransaction(.failure)
//            return
//        }
//
//        print("Updating checkout shipping address...")
//        Client.shared.updateCheckout(checkout.id, updatingCompleteShippingAddress: authorization.shippingAddress) { updatedCheckout in
//            guard let _ = updatedCheckout else {
//                completeTransaction(.failure)
//                return
//            }
//
//            print("Updating checkout email...")
//            Storefront.Client.shared.updateCheckout(checkout.id, updatingEmail: email) { updatedCheckout in
//                guard let _ = updatedCheckout else {
//                    completeTransaction(.failure)
//                    return
//                }
//
//                print("Checkout email updated: \(email)")
//                print("Completing checkout...")
//                Storefront.Client.shared.completeCheckout(checkout, billingAddress: authorization.billingAddress, applePayToken: authorization.token, idempotencyToken: paySession.identifier) { payment in
//                    if let payment = payment, checkout.paymentDue == payment.amount {
//                        print("Checkout completed successfully.")
//                        completeTransaction(.success)
//                    } else {
//                        print("Checkout failed to complete.")
//                        completeTransaction(.failure)
//                    }
//                }
//            }
//        }
//    }
//
//    func paySessionDidFinish(_ paySession: PaySession) {
//
//    }
//}

// ----------------------------------
//  MARK: - UIViewControllerPreviewingDelegate -
//

