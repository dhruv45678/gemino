//
//  GARMenuViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 15/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARMenuViewController: MenuViewController, UITableViewDelegate, UITableViewDataSource, SideMenuItemContent{

    @IBOutlet weak var tblMenu: UITableView!
    var arrMenu = ["HOME","COLLECTION","PRODUCTS","WISHLIST","MY BAG","ACCOUNT"]
    var arrImage = [#imageLiteral(resourceName: "house-outline"),#imageLiteral(resourceName: "list"),#imageLiteral(resourceName: "menu-2"),#imageLiteral(resourceName: "wishlist"),#imageLiteral(resourceName: "shopping-bag-outline-4"),#imageLiteral(resourceName: "account")]
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenu.delegate = self
        tblMenu.dataSource = self
        tblMenu.register(UINib(nibName: "GARMenuCell", bundle: nil), forCellReuseIdentifier: "GARMenuCell")
       // GARHelper().addTItle(view: self.view, value: "GARMenuViewController")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Basic Functions
    
    func prepareView()
    {

        self.navigationController?.navigationBar.topItem?.title = "GEMINO"
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.isHidden = false
        
        let sideMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-2") , style: .done, target: self, action: #selector(btnMenuClicked))
        let cartMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "shopping-bag-outline-4"), style: .done, target: self, action: #selector(btnCartClicked))
        
        self.navigationItem.leftBarButtonItem = sideMenu
        self.navigationItem.rightBarButtonItem = cartMenu
        
    }
    func goToCart()
    {
        
        let myBagVC = self.storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        myBagVC.isDirect = true
        self.navigationController?.pushViewController(myBagVC, animated: true)
    }
    
    //MARK: Button Events
    @objc func btnMenuClicked()
    {
        showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
        goToCart()
    }
    
    
    @IBAction func btnHomeClicked(_ sender: UIButton) {
        menuContainerViewController?.selectContentViewController((menuContainerViewController?.contentViewControllers[0])!)
        menuContainerViewController?.hideSideMenu()
    }
    
    @IBAction func btnSecondClicked(_ sender: UIButton) {
        menuContainerViewController?.selectContentViewController((menuContainerViewController?.contentViewControllers[1])!)
        menuContainerViewController?.hideSideMenu()
    }
    
    
    //MARK: Table Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GARMenuCell", for: indexPath) as! GARMenuCell
        cell.imgMenu.image = arrImage[indexPath.row]
        cell.lblTitle.text = arrMenu[indexPath.row]
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        menuContainerViewController?.selectContentViewController((menuContainerViewController?.contentViewControllers[indexPath.row])!)
        menuContainerViewController?.hideSideMenu()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
