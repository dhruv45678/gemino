//
//  GARProductGridViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARProductGridViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SideMenuItemContent {

    @IBOutlet weak var colProductList: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSideMenu: UIButton!
    var controllerTitle : String = "Products"
    var isBackButton : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Basic Functions
    func prepareView()
    {
        if(isBackButton)
        {
            btnSideMenu.setImage(#imageLiteral(resourceName: "previous"), for: .normal)
        }
        lblTitle.text = controllerTitle
        colProductList.delegate = self
        colProductList.dataSource = self
        colProductList.register(UINib(nibName: "GARProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARProductCollectionCell")
    }
    
    //MARK: Collection Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARProductDetailsPageViewController") as? GARProductDetailsPageViewController {
          //  viewController.product = banners[0].node.products.edges[indexPath.row]
            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARProductCollectionCell", for: indexPath) as! GARProductCollectionCell
        return cell
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2 - 20, height: 300)
    }
    
    //MARK: Button Events
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        if(isBackButton)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            showSideMenu()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
