//
//  GARCollectionViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 20/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SideMenuItemContent {

    
    @IBOutlet weak var colCollections: UICollectionView!
    @IBOutlet weak var btnSideMenu: UIButton!
    var isBackButton : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isBackButton)
        {
            btnSideMenu.setImage(#imageLiteral(resourceName: "previous"), for: .normal)
        }
        colCollections.delegate = self
        colCollections.dataSource = self
        colCollections.register(UINib(nibName: "GARListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GARListCollectionViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GARProductGridViewController") as? GARProductGridViewController {
            guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARListCollectionViewCell", for: indexPath) as! GARListCollectionViewCell
        return cell
      
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
   
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 100)
    }

    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        if(isBackButton)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            showSideMenu()
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
