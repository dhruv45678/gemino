//
//  GARWishlistViewController.swift
//  Gemino
//
//  Created by Khatri Dhruv on 20/07/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARWishlistViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SideMenuItemContent {

    @IBOutlet weak var colMyBag: UICollectionView!
    @IBOutlet weak var viewMyBag: UIView!
    
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var btnApplePay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavigation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Basic Functions
    func prepareView()
    {
        viewMyBag.isHidden = false
        GARHelper().roundCorner(view: btnCheckOut, corner: btnCheckOut.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: btnApplePay, corner: btnApplePay.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        colMyBag.delegate = self
        colMyBag.dataSource = self
        colMyBag.register(UINib(nibName: "GARMYBagCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GARMYBagCollectionViewCell")
    }
    
    func setupNavigation()
    {
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    //MARK: CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARMYBagCollectionViewCell", for: indexPath) as! GARMYBagCollectionViewCell
       
        return cell
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 180)
    }
    
    
    //MARK: Button Events
    
    @objc func btnMenuClicked()
    {
        showSideMenu()
    }
    
    @objc func btnCartClicked()
    {
        print("Cart")
    }
    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        showSideMenu()
    }
    
    
    @IBAction func btnCheckOutClicked(_ sender: UIButton) {
        GARHelper().createAlert(message: "Your Order is successfully Placed.", VC: self)
        //createCheckOut()
    }
    
    @IBAction func btnApplePayClicked(_ sender: UIButton) {
        GARHelper().createAlert(message: "Your Order is successfully Placed Using Apple Pay.", VC: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
