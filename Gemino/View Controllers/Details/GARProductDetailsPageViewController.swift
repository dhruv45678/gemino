//
//  GARProductDetailsPageViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 27/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import Firebase
import InteractiveSideMenu
import Vision
import MobileBuySDK
import SDWebImage
import SceneKit

class GARProductDetailsPageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var colProductImages: UICollectionView!
    @IBOutlet weak var btnAddToBag: UIButton!
    @IBOutlet weak var btnApplePay: UIButton!
    @IBOutlet weak var txtSize: UITextField!
    @IBOutlet weak var txtColor: UITextField!
    @IBOutlet weak var btnTryInAR: UIButton!
    @IBOutlet weak var colVeriants: UICollectionView!
    @IBOutlet weak var imgBig: UIImageView!
    @IBOutlet weak var viewBlur: UIVisualEffectView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    
    
    
    var isARAvailable : Bool = false
    var product : Storefront.ProductEdge!
    var selectedImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        isARAvailable = true
        prepareView()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Basic Functions
    func prepareView()
    {
        viewBlur.isHidden = true
        btnAddToBag.setTitle("ADD TO BAG", for: .normal)
        btnTryInAR.isHidden = !isARAvailable
        
        lblName.text = product.node.title
        lblPrice.text = "\(product.node.priceRange.maxVariantPrice.amount) \(product.node.priceRange.maxVariantPrice.currencyCode)"
        lblDescription.text = product.node.description
        
        
        colProductImages.dataSource = self
        colProductImages.delegate = self
        colProductImages.register(UINib(nibName: "GARBannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARBannerCollectionCell")
        
        colVeriants.delegate = self
        colVeriants.dataSource = self
        colVeriants.register(UINib(nibName: "GARCircleCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GARCircleCollectionCell")
        
        GARHelper().roundCorner(view: btnTryInAR, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: btnAddToBag, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: txtSize, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        GARHelper().roundCorner(view: txtColor, corner: 5.0, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        
        GARHelper().roundCorner(view: btnApplePay, corner: 5.0, borderColor: UIColor.black.cgColor, borderWidth: 1.0)
    }
    
    
    //MARK: Collection Delegate methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == colProductImages)
        {
            let cell = collectionView.cellForItem(at: indexPath) as! GARBannerCollectionCell
            imgBig.image = cell.imgBanner.image
            viewBlur.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == colVeriants)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARCircleCollectionCell", for: indexPath) as! GARCircleCollectionCell
            cell.imgCollection.sd_setImage(with: product.node.variants.edges[indexPath.row].node.image?.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
            return cell
        }
        else
        {
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GARBannerCollectionCell", for: indexPath) as! GARBannerCollectionCell
            cell.imgBanner.sd_setImage(with: product.node.images.edges[self.product.node.images.edges.count - indexPath.row - 1].node.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground) { (image, error, cache, url) in
                if(error == nil && indexPath.row == 0)
                {
                    self.selectedImage = image
                }
            }
         //   cell.imgBanner.sd_setImage(with: product.node.images.edges[indexPath.row].node.originalSrc, placeholderImage: #imageLiteral(resourceName: "ic_Logo"), options: .continueInBackground, completed: nil)
            return cell
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == colVeriants)
        {
            return 0
        }
        return product.node.images.edges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == colVeriants)
        {
            return CGSize(width: 50, height: 60)
        }
        else
        {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }

    //MARK: Button Events
    
    @IBAction func btnTryARClicked(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "GARVerticalARSurfaceViewController") as! GARVerticalARSurfaceViewController
        //controller.isDirect = true
        controller.selectedImage = selectedImage
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddToBagClicked(_ sender: UIButton) {
        if(sender.titleLabel?.text == "ADD TO BAG")
        {
            sender.setTitle("CHECK OUT", for: .normal)
            GARHelper().createAlert(message: "Successfully Added to Bag.", VC: self)
        }
        else
        {
            let controller = storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
            controller.isDirect = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func btnApplePayClicked(_ sender: UIButton) {
        GARHelper().createAlert(message: "Your Order is successfully Placed.", VC: self)
    }
    
    @IBAction func btnAddedToWishListClicked(_ sender: UIButton) {
        sender.setImage(#imageLiteral(resourceName: "like-2") ,for: .normal)
        GARHelper().createAlert(message: "Successfully Added to Wishlist.", VC: self)
    }
    
    @IBAction func btnHideClicked(_ sender: UIButton) {
        viewBlur.isHidden = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
