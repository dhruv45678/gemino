//
//  GARLoginViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 27/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARLoginViewController: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GARHelper().roundCorner(view: btnLogin, corner: btnLogin.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Basic Functions
    func goToHome()
    {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "GARHostViewController") as! GARHostViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func goToSignUp()
    {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "GARSignUpViewController") as! GARSignUpViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    
    //MARK: Button Events
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        goToHome()
    }
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        goToSignUp()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
