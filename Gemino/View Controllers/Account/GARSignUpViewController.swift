//
//  GARSignUpViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 27/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit

class GARSignUpViewController: UIViewController {

    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GARHelper().roundCorner(view: btnSignup, corner: btnSignup.frame.height/2, borderColor: UIColor.clear.cgColor, borderWidth: 0.0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goToHome()
    {
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "GARHostViewController") as! GARHostViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
   
    //MARK: Button Events
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        goToHome()
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
