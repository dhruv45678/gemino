//
//  GARHostViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 15/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu

class GARHostViewController: MenuContainerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      //  GARHelper().addTItle(view: self.view, value: "GARHostViewController")
        let screenSize: CGRect = UIScreen.main.bounds
        self.transitionOptions = TransitionOptions(duration: 0.4, visibleContentWidth: screenSize.width / 6)
        
        // Instantiate menu view controller by identifier
        
        let menuVC = storyboard?.instantiateViewController(withIdentifier: "GARMenuViewController") as! GARMenuViewController
        self.menuViewController = menuVC
        
        // Gather content items controllers
        self.contentViewControllers = contentControllers()
        
        // Select initial content controller. It's needed even if the first view controller should be selected.
        self.selectContentViewController(contentViewControllers.first!)
        
        self.currentItemOptions.cornerRadius = 10.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Options to customize menu transition animation.
        var options = TransitionOptions()
        
        // Animation duration
        options.duration = size.width < size.height ? 0.4 : 0.6
        
        // Part of item content remaining visible on right when menu is shown
        options.visibleContentWidth = size.width / 6
        self.transitionOptions = options
    }
    
    private func contentControllers() -> [UIViewController] {
        
        
        let mainTabBarController = storyboard?.instantiateViewController(withIdentifier: "GARTabNavigationViewController") as! GARTabNavigationViewController
        let productGridViewController = storyboard?.instantiateViewController(withIdentifier: "GARProductGridViewController") as! GARProductGridViewController
        let myBagViewController = storyboard?.instantiateViewController(withIdentifier: "GARMyBagViewController") as! GARMyBagViewController
        //myBagViewController.isDirect = true
        let collectionViewController = storyboard?.instantiateViewController(withIdentifier: "GARCollectionViewController") as! GARCollectionViewController
        
        let wishlistViewController = storyboard?.instantiateViewController(withIdentifier: "GARWishlistViewController") as! GARWishlistViewController
        let profileViewController = storyboard?.instantiateViewController(withIdentifier: "GARProfileViewController") as! GARProfileViewController
        return [mainTabBarController,collectionViewController, productGridViewController, wishlistViewController , myBagViewController, profileViewController]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
