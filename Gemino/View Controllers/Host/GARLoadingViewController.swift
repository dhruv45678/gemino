//
//  GARLoadingViewController.swift
//  Gemino
//
//  Created by dhruv Khatri on 12/06/18.
//  Copyright © 2018 dhruv Khatri. All rights reserved.
//

import UIKit
import InteractiveSideMenu
class GARLoadingViewController: UIViewController, SideMenuItemContent {

    @IBOutlet weak var imgLanding: UIImageView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: API Calls
    
    func getLnadingImage()
    {
        Firebase.child(Key.APPID).child(Key.Profile).observeSingleEvent(of: .value) { (datashot) in
            if let profileDic = datashot.value as? NSDictionary
            {
                if let imageLink = profileDic[Key.ImageURL] as? String
                {
                    print(imageLink)
                }
                if let color = profileDic[Key.Color] as? NSDictionary
                {
                    if let R = color[Key.RedColor] as? Int, let G = color[Key.GreenColor] as? Int, let B = color[Key.BlueColor] as? Int
                    {
                        AppColor = UIColor(red: CGFloat(R/255), green: CGFloat(G/255), blue: CGFloat(B/255), alpha: 1.0)
                    }
                }
                if let arT = profileDic[Key.ARType] as? String
                {
                    arType = arT
                }
            }
            
        }
    }
    
    func getMenuOrder()
    {
        Firebase.child("APP_ID").child("menuList").observeSingleEvent(of: .value) { (datashot) in
            if let menuListDic = datashot.value as? NSDictionary
            {
                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "GARLoginViewController") as! GARLoginViewController
                arrHome = menuListDic["menu"] as! [String]
                arrHome.append("AR")
                self.navigationController?.pushViewController(loginVC, animated: true)
                print(menuListDic["menu"] as! [String])
            }
        }
    }

    
    //MARK: Basic functions
    func prepareView()
    {
        //GARHelper().addTItle(view: self.view, value: "GARLoadingViewController")
        self.navigationController?.navigationBar.isHidden = true
        getMenuOrder()
        getLnadingImage()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
